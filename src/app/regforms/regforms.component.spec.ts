import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegformsComponent } from './regforms.component';

describe('RegformsComponent', () => {
  let component: RegformsComponent;
  let fixture: ComponentFixture<RegformsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegformsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegformsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
