import { Component, OnInit } from '@angular/core';
import {FormGroup, FormControl , Validators } from '../../../node_modules/@angular/forms';
import { MyserviceService } from '../myservice.service';

@Component({
  selector: 'app-forms',
  templateUrl: './forms.component.html',
  styleUrls: ['./forms.component.css']
})
export class FormsComponent implements OnInit {

  constructor(private myservice: MyserviceService) { }
  formdata = '';
  date ;
  ngOnInit() {
    this.date = this.myservice.showTodayDate();
  }
  onClickSubmit(data) {

    console.log('Entered Email id : ' + data.email);
    alert('Entered Password : ' + data.passwd);
 }
}
