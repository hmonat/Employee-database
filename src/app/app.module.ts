import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import {APP_BASE_HREF} from '@angular/common';
import { MyserviceService } from './myservice.service';

import { AppComponent } from './app.component';
import { JavascriptComponent } from './javascript/javascript.component';
import { PhpComponent } from './php/php.component';
import { KukuDirective } from './kuku.directive';
import { CominteractionComponent } from './cominteraction/cominteraction.component';
import { FormsComponent } from './forms/forms.component';
import { RegformsComponent } from './regforms/regforms.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ReactiveformComponent } from './reactiveform/reactiveform.component';



@NgModule({
  declarations: [
    AppComponent,
    JavascriptComponent,
    PhpComponent,
    KukuDirective,
    CominteractionComponent,
    FormsComponent,
    RegformsComponent,
    ReactiveformComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot([
      {
         path: 'new-cmp',
         component: JavascriptComponent
      }
   ]),
   ReactiveFormsModule,
  // FormGroup, FormControl, Validators
  ],
  providers: [{provide: APP_BASE_HREF, useValue : '/' }, MyserviceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
