import { Component, OnInit } from '@angular/core';
import {FormGroup, FormControl , Validators } from '../../../node_modules/@angular/forms';
import { MyserviceService } from '../myservice.service';

@Component({
  selector: 'app-reactiveform',
  templateUrl: './reactiveform.component.html',
  styleUrls: ['./reactiveform.component.css']
})
export class ReactiveformComponent implements OnInit {
emailid = '';
  constructor(private myservice: MyserviceService) { }
  formdata;
  date ;
  ngOnInit() {
   this.formdata = new FormGroup({
    emailid: new FormControl('', Validators.compose([
       Validators.required,
       Validators.pattern('[^ @]*@[^ @]*')
    ])),
    passwd: new FormControl('')
 });
 this.date = this.myservice.showTodayDate();
  }
  onClickSubmit(data) {this.emailid = data.emailid; }
}
