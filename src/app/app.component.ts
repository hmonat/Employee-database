import { Component } from '@angular/core';
import { $ } from '../../node_modules/protractor';
import { MyserviceService } from './myservice.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'angular';
  editable = '';
  todaydate = new Date();
  Name = '';

  kuch(val) {

    if ( val === '' || val == null) {
      document.getElementById(  'title' ).innerHTML = 'Document';
    } else {
      document.getElementById('title').innerHTML = val;
    }
  }


  changeContent() {
   const row = prompt('enter row', '');
   const col = prompt('enter col', '');
   const content = prompt('enter content', '');
   document.getElementById('row_' + row + 'col_' + col).innerHTML = content;
  }
}

