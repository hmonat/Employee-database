import { Directive , ElementRef} from '@angular/core';

@Directive({
  selector: '[appKuku]'
})
export class KukuDirective {

  constructor(e: ElementRef ) {
    e.nativeElement.style.backgroundColor = 'orange';
    e.nativeElement.style.color = 'red';
    e.nativeElement.style.textAlign = 'center';
    e.nativeElement.outerText = ' Text is Replaced ' ;
   }

}
