import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CominteractionComponent } from './cominteraction.component';

describe('CominteractionComponent', () => {
  let component: CominteractionComponent;
  let fixture: ComponentFixture<CominteractionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CominteractionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CominteractionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
